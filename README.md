# Notebook-CSAPP

计算机系统

## [从hello world讲起](ch1/README.md)

* [信息](ch1/README.md#信息)
* [编译](ch1/README.md#编译)
* [处理器执行指令](ch1/README.md#处理器执行指令)
* [高速缓存](ch1/README.md#高速缓存)
* [存储层次](ch1/README.md#存储层次)
* [操作系统](ch1/README.md#操作系统)
* [IPC(进程间通信)](ch1/README.md#ipc进程间通信)
  
## [信息的表示和处理](ch2/README.md)

* [信息存储](ch2/2.1.md)
* [整数](ch2/2.2.md)
* [整数运算](ch2/2.3.md)
* [浮点数](ch2/2.4.md)
* [字符编码](ch2/2.5.md)
  
## [程序的表示](ch3/README.md)

* 程序编码
* 数据格式
* 存取指令
* 运算指令
* 条件控制指令
* 过程调用
* 数组
* 数据结构
* 指针与溢出
* 浮点数

## [处理器体系结构](ch4/README.md)

## [存储器层次结构](ch6/README.md)

* [存储器技术](ch6/6.1.md)
* [存储器层次](ch6/6.2.md)
* [高速缓存](ch6/6.3.md)

## [链接](ch7/README.md)

## [异常控制流](ch8/README.md)

## [虚拟存储器](ch9/README.md)

## [I/O](ch10/README.md)

## [网络](ch11/README.md)

## [并发](ch12/README.md)
